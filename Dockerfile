FROM alpine:latest

COPY requirements.txt /
COPY LICENSE.txt pipe.yml README.md /usr/bin/

RUN apk add --no-cache \
      bash \
      python3 \
      py3-pip \
      jq \
      git \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir -r requirements.txt \
    && rm -rf /var/cache/apk/*

COPY src /usr/bin/

ENTRYPOINT ["python3", "/usr/bin/pipe.py"]

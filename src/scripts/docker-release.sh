#!/usr/bin/env bash
#
# Release to Bintray.
#
# On master, we push both a :latest tag, and :<next-version>
# On release/<foo> branches, we only push :<next-version>
#
# Required globals:
#   AWS_DEFAULT_REGION
#   BITBUCKET_BRANCH
#   BITBUCKET_BUILD_NUMBER
#
# Arguments:
#   REPOSITORY: The AWS ECR public repository we are pushing to.
#
set -ex

REPOSITORY=$1
VERSION=${VERSION:-$(semversioner current-version)}

# Login to docker registry on AWS
docker_login() {
    aws ecr-public get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $DOCKER_ECR_REPO_URL
}

# Generate all tags.
generate_tags() {
    if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
        tags=("latest" "${VERSION}")
    elif [[ "${BITBUCKET_BRANCH}" =~ "qa-" ]]; then
        tags=("${VERSION}-qa-${BITBUCKET_BUILD_NUMBER}")
    else
        tags=("${VERSION}")
    fi
}

# Build and tag with an intermediate tag.
docker_build() {
    docker build -t ${REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER} .
}

# Tag with final tags and push.
docker_push() {
    for tag in "${tags[@]}"; do
        docker tag "${REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${REPOSITORY}:${tag}"
        docker push "${REPOSITORY}:${tag}"
    done
}

docker_login
generate_tags
docker_build
docker_push

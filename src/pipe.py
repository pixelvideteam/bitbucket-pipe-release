import glob
import re
import os
import subprocess
import stat

from bitbucket_pipes_toolkit import Pipe

SCHEMA = {
    # GLOBALS
    'BITBUCKET_BRANCH': {'required': True, 'type': 'string'},
    'BITBUCKET_GIT_SSH_ORIGIN': {'required': True, 'type': 'string'},
    'BITBUCKET_GIT_HTTP_ORIGIN': {'required': True, 'type': 'string'},
    'BITBUCKET_BUILD_NUMBER': {'required': True, 'type': 'integer'},

    # AWS-SPECIFIC
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},

    # DOCKER-SPECIFIC
    'DOCKER_ECR_REPO_URL': {'type': 'string', 'required': True},
    'DOCKER_IMAGE_NAME': {'type': 'string', 'required': True},

    # PIPE-SPECIFIC
    'CHANGELOG': {'required': False, 'type': 'boolean', 'default': True},
    'UPDATE_VERSION_IN_FILES': {'required': False, 'type': 'string', 'default': 'README.md pipe.yml'},
    'VERSION': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'GIT_PUSH': {'required': False, 'type': 'boolean', 'default': True},
    'GIT_USER': {'required': False, 'type': 'string', 'default': 'Bitbucket Pipelines Push Bot'},
    'SSH_KEY': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'TAG': {'required': False, 'type': 'boolean', 'default': True},
}


def replace_content_by_pattern(filename, replacement_string, pattern):
    with open(filename) as f:
        content = f.read()

    with open(filename, 'w') as f:
        new_content = re.sub(re.compile(pattern), replacement_string, content)
        f.write(new_content)


class Release(Pipe):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        docker_ecr_repo_url = self.get_variable('DOCKER_ECR_REPO_URL')
        self.image = self.get_variable('DOCKER_IMAGE_NAME')
        self.docker_repository = f'{docker_ecr_repo_url}/{self.image}'

        self._script_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'scripts')
        self._allow_executable()

    def run(self):
        super().run()

        version = self.version_bump()
        self.docker_release()

        if self.get_variable('GIT_PUSH'):
            self.log_info(f'Committing version {version} to git')
            self.git_commit(version)

        if self.get_variable('TAG') and self.get_variable('GIT_PUSH'):
            self.log_info(f'Tag version {version} to git')
            self.git_tag(version)

        self.success('Release pipe finished successfully')

    def _allow_executable(self):
        for path in glob.iglob(os.path.join(self._script_path, '*.sh')):
            os.chmod(path, os.stat(path).st_mode | stat.S_IEXEC)

    def version_bump(self):
        """Bump a version

        Bump a version, update necessary files, generate changelog
        """
        # TODO: get rid of subprocess calling, once semversioner supports python functions outside
        version = self.get_variable('VERSION')
        if not version:
            try:
                subprocess.run(['semversioner', 'release'], check=True, capture_output=True, text=True)
                result = subprocess.run(['semversioner', 'current-version'], check=True, capture_output=True, text=True)
                version = result.stdout.strip('\n')
            except subprocess.CalledProcessError as exc:
                self.fail(f'{exc.output} {exc.stderr}')

        if self.get_variable('CHANGELOG'):
            self.log_info('Generating CHANGELOG.md file...')
            try:
                result = subprocess.run(['semversioner', 'changelog'], check=True, capture_output=True, text=True)
                with open('CHANGELOG.md', 'w') as f:
                    f.write(result.stdout)
            except subprocess.CalledProcessError as exc:
                self.fail(f'{exc.output} {exc.stderr}')

        filenames_to_update = self.get_variable('UPDATE_VERSION_IN_FILES').split(' ')
        if filenames_to_update == '':
            return

        for filename in filenames_to_update:
            repo_slug = self.image.split('/')[-1]
            replace_content_by_pattern(filename, f'{repo_slug}:{version}',
                                       f'{repo_slug}:[0-9]*\.[0-9]*\.[0-9]*(@sha256:[a-fA-F0-9]*)?')  # noqa

        return version

    def docker_release(self):
        self.log_info('releasing docker image...')
        script = os.path.join(self._script_path, 'docker-release.sh')
        try:
            subprocess.run([script, self.docker_repository], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')

    def git_commit(self, version):
        script = os.path.join(self._script_path, 'git-commit.sh')
        try:
            subprocess.run([script, version], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')

    def git_tag(self, version):
        script = os.path.join(self._script_path, 'git-tag.sh')
        try:
            subprocess.run([script, version], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')


def main():
    release = Release(schema=SCHEMA)
    release.run()


if __name__ == '__main__':
    main()

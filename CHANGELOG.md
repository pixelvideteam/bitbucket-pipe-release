# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.2

- patch: bitbucket pipeline Base Image changed

## 0.1.1

- patch: Base Image changed to Alphine

## 0.1.0

- minor: Initial release
